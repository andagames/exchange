package com.exchange.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchange.model.Transaction;
import com.exchange.repository.TransactionRepository;

/**
 * @author Cemal Doğukan
 *
 */
@Service
public class TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	public Transaction getTransactionById(int id) {
		return transactionRepository.findById(id).get();
	}

	public void saveOrUpdate(Transaction transaction) {
		transactionRepository.save(transaction);
	}

	public void delete(int id) {
		transactionRepository.deleteById(id);
	}

}
