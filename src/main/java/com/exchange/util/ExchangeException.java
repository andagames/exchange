package com.exchange.util;

/**
 * @author Cemal Doğukan
 *
 */
public class ExchangeException extends Exception {

	private static final long serialVersionUID = 9058640774846047103L;

	private final int code;

	public ExchangeException(int code) {
		super();
		this.code = code;
	}

	public ExchangeException(String message, Throwable cause, int code) {
		super(message, cause);
		this.code = code;
	}

	public ExchangeException(String message, int code) {
		super(message);
		this.code = code;
	}

	public ExchangeException(Throwable cause, int code) {
		super(cause);
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}
}
