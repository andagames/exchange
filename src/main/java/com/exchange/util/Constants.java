package com.exchange.util;

/**
 * @author Cemal Doğukan
 *
 */
public class Constants {

	public static final String URL_LINK = "http://data.fixer.io/api/";
	public static final String ACCESS_KEY = "?access_key=8ceee749cb2b0814926713fc37c13375";
	public static final String FORMAT = "&format=1";
	public static final String SYMBOLS = "&symbols=USD,AUD,CAD,PLN,MXN";
	public static final String FROM = "&from=";
	public static final String TO = "&to=";
	public static final String AMOUNT = "&amount=";

	
}
