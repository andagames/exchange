package com.exchange.response;

import java.util.List;

import com.exchange.model.CurrencyDTO;

/**
 * @author Cemal Doğukan
 *
 */
public class ResponseCurrencyByDate {
	private List<CurrencyDTO> currencyList;
	private String base;

	public List<CurrencyDTO> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<CurrencyDTO> currencyList) {
		this.currencyList = currencyList;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}
}
