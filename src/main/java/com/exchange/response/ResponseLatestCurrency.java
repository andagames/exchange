package com.exchange.response;

import java.util.List;
import com.exchange.model.CurrencyDTO;

/**
 * @author Cemal Doğukan
 *
 */
public class ResponseLatestCurrency {

	private List<CurrencyDTO> currencyList;
	private String rawResult;

	public List<CurrencyDTO> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<CurrencyDTO> currencyList) {
		this.currencyList = currencyList;
	}

	public String getRawResult() {
		return rawResult;
	}

	public void setRawResult(String rawResult) {
		this.rawResult = rawResult;
	}
}
