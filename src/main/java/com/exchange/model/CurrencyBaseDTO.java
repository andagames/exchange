package com.exchange.model;

import java.util.Date;
import java.util.List;

/**
 * @author Cemal Doğukan
 *
 */
public class CurrencyBaseDTO {

	private List<CurrencyDTO> currencyList;
	private String base;
	private Date date;

	public List<CurrencyDTO> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<CurrencyDTO> currencyList) {
		this.currencyList = currencyList;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
