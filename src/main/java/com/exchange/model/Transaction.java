package com.exchange.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.Digits;

/**
 * @author Cemal Doğukan
 *
 */
@Entity
public class Transaction {

	@Id
	@GeneratedValue
	private int id;
	private String transactionType;
	private String fromCurrency;
	private String toCurrency;
	@Column(nullable= false, precision=7, scale=2)    // Creates the database field with this size.
	@Digits(integer=9, fraction=2)                    // Validates data when used as a form
	private BigDecimal amount;
	private String result;
	private String baseCurrency;
	private String currencyDate;
	private Date transactionDate;
	private String userName;
	@Lob
	private byte[] transactionInformation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getFromCurrency() {
		return fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getCurrencyDate() {
		return currencyDate;
	}

	public void setCurrencyDate(String currencyDate) {
		this.currencyDate = currencyDate;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public byte[] getTransactionInformation() {
		return transactionInformation;
	}

	public void setTransactionInformation(byte[] transactionInformation) {
		this.transactionInformation = transactionInformation;
	}
}
