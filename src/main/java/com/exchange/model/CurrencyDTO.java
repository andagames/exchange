package com.exchange.model;

/**
 * @author Cemal Doğukan
 *
 */
public class CurrencyDTO {

	private String currency;
	private Object value;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
