package com.exchange.repository;

import org.springframework.data.repository.CrudRepository;

import com.exchange.model.User;

/**
 * @author Cemal Doğukan
 *
 */
public interface UserRepository extends CrudRepository<User, Integer> {
}
