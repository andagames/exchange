package com.exchange.repository;

import org.springframework.data.repository.CrudRepository;

import com.exchange.model.Transaction;

/**
 * @author Cemal Doğukan
 *
 */
public interface TransactionRepository extends CrudRepository<Transaction, Integer> {
}
