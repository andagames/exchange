package com.exchange.controller;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.config.CurrencyApi;
import com.exchange.model.CurrencyDTO;
import com.exchange.model.Transaction;
import com.exchange.response.ResponseLatestCurrency;
import com.exchange.service.TransactionService;
import com.exchange.util.ExchangeException;

/**
 * @author Cemal Doğukan
 *
 */
@Transactional
@RestController
@RequestMapping("/api")
public class ConversionController {

	private final Logger LOG = LoggerFactory.getLogger(ConversionController.class);

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private CurrencyApi api;

	@RequestMapping(value = "/convert", produces = "application/json")
	public String getLatestTest(@RequestParam String from, @RequestParam String to, @RequestParam String amount)
			throws ExchangeException {
		Double result = convert(from, to, amount);
		System.out.println(result);
		return result.toString();
	}

	private double convert(String fromCurrency, String toCurrency, String amount) throws ExchangeException {

		// since fixerIO api only allows(free) EUR based currency inquiry, it is written
		// manually.
		ResponseLatestCurrency response = api.getLatestCurrency("EUR");
		double toEURValue = 0;
		double fromEURValue = 0;

		double result = 0;
		for (CurrencyDTO cur : response.getCurrencyList()) {

			if (cur.getCurrency().equals(fromCurrency))
				fromEURValue = (Double) cur.getValue();
			if (cur.getCurrency().equals(toCurrency))
				toEURValue = (Double) cur.getValue();

		}
		double doubleAmount = (double) Integer.parseInt(amount);
		result = doubleAmount * (1 / fromEURValue) * (toEURValue);

		Transaction transaction = new Transaction();
		transaction.setAmount(BigDecimal.valueOf(doubleAmount));
		transaction.setBaseCurrency("EUR");
		transaction.setFromCurrency(fromCurrency);
		transaction.setToCurrency(toCurrency);
		transaction.setResult(String.valueOf(doubleAmount).toString());
		transaction.setTransactionDate(new Date());
		transaction.setTransactionType("Conversion");
		transaction.setUserName("system");
		transaction.setTransactionInformation(convertDoubleToByteArray(result));
		transactionService.saveOrUpdate(transaction);
		LOG.info("convert transaction saved with following info: " + "\n" + "From Currency: " + fromCurrency);
		return result;

	}

	private byte[] convertDoubleToByteArray(double number) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES);
		byteBuffer.putDouble(number);
		return byteBuffer.array();
	}
}
