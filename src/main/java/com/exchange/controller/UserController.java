package com.exchange.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.model.User;
import com.exchange.service.UserService;

/**
 * @author Cemal Doğukan
 *
 */
@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("/users")
	private List<User> getAllUsers() {

		return userService.getAllUsers();
	}

	@GetMapping("/users/{id}")
	private User getUser(@PathVariable("id") int id) {
		return userService.getUserById(id);
	}

	@DeleteMapping("/users/{id}")
	private void deleteUser(@PathVariable("id") int id) {
		userService.delete(id);
	}

	@PostMapping("/users/define")
	private int saveUser(@RequestBody User user) {
		userService.saveOrUpdate(user);
		return user.getId();
	}
}