package com.exchange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.model.Transaction;
import com.exchange.service.TransactionService;

/**
 * @author Cemal Doğukan
 *
 */
@Transactional
@RestController
@RequestMapping("/api")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@GetMapping("/transaction/{id}")
	private Transaction getTransactionById(@PathVariable("id") int id) {
		return transactionService.getTransactionById(id);
	}

	@DeleteMapping("/transaction/{id}")
	private void deleteTransaction(@PathVariable("id") int id) {
		transactionService.delete(id);
	}

	@PostMapping("/transaction/define")
	private Transaction savePerson(@RequestBody Transaction transaction) {
		transactionService.saveOrUpdate(transaction);
		return transaction;
	}
}
