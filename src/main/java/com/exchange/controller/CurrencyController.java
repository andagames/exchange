package com.exchange.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.h2.api.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.config.ConnectionGenerator;
import com.exchange.config.CurrencyApi;
import com.exchange.model.CurrencyBaseDTO;
import com.exchange.model.CurrencyDTO;
import com.exchange.model.Transaction;
import com.exchange.response.ResponseLatestCurrency;
import com.exchange.service.TransactionService;
import com.exchange.util.Constants;
import com.exchange.util.ExchangeException;

/**
 * @author Cemal Doğukan
 *
 */
@Transactional
@RestController
@RequestMapping("/api/currency")
public class CurrencyController {

	private final Logger LOG = LoggerFactory.getLogger(CurrencyController.class);

	@Autowired
	private ConnectionGenerator connectionGenerator = new ConnectionGenerator();

	@Autowired
	private CurrencyApi api;

	@Autowired
	private TransactionService transactionService;

	@RequestMapping(value = "/latest", produces = "application/json")
	public CurrencyBaseDTO getLatest(@RequestParam(required = false) String base) throws ExchangeException {
		if (base == null)
			base = "EUR";

		ResponseLatestCurrency response = api.getLatestCurrency(base);
		CurrencyBaseDTO dto = convertToRequestedBase(response, base);
		Transaction transaction = new Transaction();
		transaction.setAmount(null);
		transaction.setBaseCurrency(dto.getBase());
		transaction.setFromCurrency("");
		transaction.setToCurrency("");
		transaction.setResult("");
		transaction.setTransactionDate(new Date());
		transaction.setTransactionType("Inquiry");
		transaction.setUserName("system");
		transaction.setTransactionInformation(response.getRawResult().getBytes());
		transactionService.saveOrUpdate(transaction);

		return dto;
	}

	@RequestMapping(value = "/latest/five", produces = "application/json")
	public String getRates() throws MalformedURLException, ExchangeException {
		try {
			String path = "latest";
			HttpURLConnection con = connectionGenerator.generateLink(path, Constants.SYMBOLS);
			return connectionGenerator.createConnection(con);
		} catch (IOException e) {
			LOG.error("CurrencyController | could not gathered latest currency values symbol rates");
			throw new ExchangeException("Connection problem with source api while getting rates.", e,
					ErrorCode.CONNECTION_BROKEN_1);
		}
	}

	@RequestMapping(value = "/date", produces = "application/json")
	public CurrencyBaseDTO getCurrencyByDate(@RequestParam(required = false) String base, @RequestParam String date)
			throws MalformedURLException, ExchangeException {
		if (base == null)
			base = "EUR";
		ResponseLatestCurrency response = api.getCurrencyByDate(base, date);
		CurrencyBaseDTO dto = convertToRequestedBase(response, base);
		dto.setDate(convertDate(date));
		Transaction transaction = new Transaction();
		transaction.setAmount(null);
		transaction.setBaseCurrency(base);
		transaction.setFromCurrency("");
		transaction.setToCurrency("");
		transaction.setResult("");
		transaction.setTransactionDate(new Date());
		transaction.setTransactionType("InquiryByDate");
		transaction.setUserName("system");
		transaction.setTransactionInformation(response.getRawResult().getBytes());
		transactionService.saveOrUpdate(transaction);
		return dto;

	}

	private Date convertDate(String date) throws ExchangeException {
		Date convertedDate;
		try {
			convertedDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
			return convertedDate;
		} catch (ParseException e) {
			LOG.error("CurrencyController | could not converted date.");
			throw new ExchangeException("Could not converted date, please use 'yyyy-MM-dd' format", e,
					ErrorCode.INVALID_TO_DATE_FORMAT);
		}
	}

	private CurrencyBaseDTO convertToRequestedBase(ResponseLatestCurrency response, String base) {
		CurrencyBaseDTO currencyBaseList = new CurrencyBaseDTO();
		double baseValue = 0;
		double tempValue = 0;
		Object baseValueObject;

		for (CurrencyDTO cur : response.getCurrencyList()) {
			if (cur.getCurrency().equals(base)) {
				baseValueObject = cur.getValue();
				baseValue = (double) Double.parseDouble(baseValueObject.toString());
				break;
			}
		}
		List<CurrencyDTO> newCurrencyList = new ArrayList<CurrencyDTO>();
		for (CurrencyDTO dto : response.getCurrencyList()) {
			CurrencyDTO newBaseCurrencyDTO = new CurrencyDTO();
			tempValue = baseValue / ((double) Double.parseDouble(dto.getValue().toString()));
			newBaseCurrencyDTO.setCurrency(dto.getCurrency());
			newBaseCurrencyDTO.setValue(tempValue);
			newCurrencyList.add(newBaseCurrencyDTO);
		}
		currencyBaseList.setBase(base);
		currencyBaseList.setCurrencyList(newCurrencyList);
		return currencyBaseList;
	}

}
