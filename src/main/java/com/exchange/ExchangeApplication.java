package com.exchange;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import com.exchange.controller.CurrencyController;

/**
 * @author Cemal Doğukan
 *
 */

@ComponentScan
@SpringBootApplication
public class ExchangeApplication {

	@Autowired
	RestTemplate restTemplate;
	
	private final Logger LOG = LoggerFactory.getLogger(ExchangeApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ExchangeApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			String[] beanNames = ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				LOG.info(beanName);
			}

		};
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
