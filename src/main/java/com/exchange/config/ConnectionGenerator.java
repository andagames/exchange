package com.exchange.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.exchange.ExchangeApplication;
import com.exchange.util.Constants;

/**
 * @author Cemal Doğukan
 *
 */
@Service
public class ConnectionGenerator {

	private final Logger LOG = LoggerFactory.getLogger(ConnectionGenerator.class);

	public String createConnection(HttpURLConnection con) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return response.toString();
	}

	public HttpURLConnection generateLink(String path, String symbols)
			throws MalformedURLException, IOException, ProtocolException {
		String currentLink = Constants.URL_LINK + path + Constants.ACCESS_KEY + symbols + Constants.FORMAT;

		URL url = new URL(currentLink);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.addRequestProperty("User-Agent", "Chrome");
		LOG.info(currentLink);
		return con;
	}

	public HttpURLConnection generateConversionLink(String path, String from, String to, String amount)
			throws MalformedURLException, IOException, ProtocolException {
		String currentLink = Constants.URL_LINK + path + Constants.FROM + from + Constants.TO + to + Constants.AMOUNT
				+ amount + Constants.ACCESS_KEY + Constants.FORMAT;

		URL url = new URL(currentLink);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.addRequestProperty("User-Agent", "Chrome");
		LOG.info(currentLink);
		return con;
	}

}
