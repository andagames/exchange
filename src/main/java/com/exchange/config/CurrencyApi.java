package com.exchange.config;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.h2.api.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Service;

import com.exchange.model.CurrencyDTO;
import com.exchange.response.ResponseLatestCurrency;
import com.exchange.util.ExchangeException;

/**
 * @author Cemal Doğukan
 *
 */
@Service
public class CurrencyApi {

	private final Logger LOG = LoggerFactory.getLogger(CurrencyApi.class);

	@Autowired
	private ConnectionGenerator connectionGenerator;

	public ResponseLatestCurrency getLatestCurrency(String base) throws ExchangeException {
		try {
			String path = "latest";
			HttpURLConnection con = connectionGenerator.generateLink(path, "");
			String resp = connectionGenerator.createConnection(con);
			JsonParser springParser = JsonParserFactory.getJsonParser();
			Map<String, Object> map = springParser.parseMap(resp);

			String mapArray[] = new String[map.size()];
			// LOG.info("Items found: " + mapArray.length);
			ResponseLatestCurrency response = new ResponseLatestCurrency();
			response.setRawResult(resp);
			List<CurrencyDTO> currencyList = new ArrayList<CurrencyDTO>();
			for (Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().equals("rates")) {
					for (Entry<String, Object> rateMap : ((Map<String, Object>) entry.getValue()).entrySet()) {
						CurrencyDTO dto = new CurrencyDTO();
						dto.setCurrency(rateMap.getKey());
						dto.setValue((Object) rateMap.getValue());
						currencyList.add(dto);
						// LOG.info(rateMap.getKey() + " = " + rateMap.getValue());
					}
				}
			}
			response.setCurrencyList(currencyList);
			return response;

		} catch (IOException e) {
			LOG.error("ExchangeController | could not gathered latest currency values for all currency rates");
			throw new ExchangeException("Connection problem with source api while getting latest currency.", e,
					ErrorCode.CONNECTION_BROKEN_1);
		}

	}

	public ResponseLatestCurrency getCurrencyByDate(String base, String date) throws ExchangeException {
		try {

			HttpURLConnection con = connectionGenerator.generateLink(date, "");
			String resp = connectionGenerator.createConnection(con);
			JsonParser springParser = JsonParserFactory.getJsonParser();
			Map<String, Object> map = springParser.parseMap(resp);
			String mapArray[] = new String[map.size()];
			// LOG.info("Items found: " + mapArray.length);
			ResponseLatestCurrency response = new ResponseLatestCurrency();
			response.setRawResult(resp);
			List<CurrencyDTO> currencyList = new ArrayList<CurrencyDTO>();
			for (Entry<String, Object> entry : map.entrySet()) {
				if (entry.getKey().equals("rates")) {
					for (Entry<String, Object> rateMap : ((Map<String, Object>) entry.getValue()).entrySet()) {
						CurrencyDTO dto = new CurrencyDTO();
						dto.setCurrency(rateMap.getKey());
						dto.setValue((Object) rateMap.getValue());
						currencyList.add(dto);
						// LOG.info(rateMap.getKey() + " = " + rateMap.getValue());
					}
				}
			}
			response.setCurrencyList(currencyList);
			return response;

		} catch (IOException e) {
			LOG.error("CurrencyApi | could not gathered currency values by date");
			throw new ExchangeException("Connection problem with source api while getting currency by date.", e,
					ErrorCode.CONNECTION_BROKEN_1);
		}

	}
}
